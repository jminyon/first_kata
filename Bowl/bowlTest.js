describe("Bowling Score", function (){
	describe("perfect game", function(){
		it("gives perfect score", function(){
			expect(bowlScore(perfscores)).toEqual(300);
		});
	});
	describe("average game", function(){
		it("gives average score", function(){
			expect(bowlScore(avgscores)).toEqual(151);
		});
	});
	describe("terrible game", function(){
		it("gives bad score", function(){
			expect(bowlScore(badscores)).toEqual(0);
		});

	});
});